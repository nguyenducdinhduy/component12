<?php /*========================================
navi
================================================*/ ?>
<div class="c-dev-title1">navi</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi1</div>
<div class="c-icon">
    <span class="c-icon__line"></span>
    <span class="c-icon__line"></span>
    <span class="c-icon__line"></span>
</div>
<br>
<nav class="c-navi">
    <ul>
        <li class="c-navi__sub">
            <p class="c-navi__item"><a href="#"></a>新明電材とは</p>
            <div class="c-navi__child">
                <div class="c-navi__child__title">新明電材とは</div>
                <div class="l-container">
                    <div>
                        <p><a href="#">私たちの役割</a></p>
                    </div>
                    <div>
                        <p><a href="#">歴史と成長</a></p>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-navi__sub">
            <p class="c-navi__item"><a href="#"></a>取扱商品・メーカー</p>
            <div class="c-navi__child aaa">
                <div class="c-navi__child__title">取扱商品・メーカー</div>
                <div class="l-container">
                    <div>
                        <p><a href="#">電線及び電線管付属品</a></p>
                    </div>
                    <div>
                        <p><a href="#">電設機器</a></p>
                    </div>
                    <div>
                        <p><a href="#">配線器具</a></p>
                    </div>
                    <div>
                        <p><a href="#">照明器具</a></p>
                    </div>
                    <div>
                        <p><a href="#">制御機器</a></p>
                    </div>
                    <div>
                        <p><a href="#">情報通信機器</a></p>
                    </div>
                    <div>
                        <p><a href="#">防災システム</a></p>
                    </div>
                    <div>
                        <p><a href="#">空調設備</a></p>
                    </div>
                    <div>
                        <p><a href="#">環境システム・住宅設備</a></p>
                    </div>
                    <div>
                        <p><a href="#">電設工具・計器</a></p>
                    </div>
                    <div>
                        <p><a href="#">その他</a></p>
                    </div>
                    <div>
                        <p><a href="#">取扱メーカ一覧</a></p>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-navi__sub">
            <p class="c-navi__item"><a href="#"></a>企業情報</p>
            <div class="c-navi__child">
                <div class="c-navi__child__title">企業情報</div>
                <div class="l-container">
                    <div>
                        <p><a href="#">企業情報</a></p>
                    </div>
                    <div>
                        <p><a href="#">ごあいさつ</a></p>
                    </div>
                    <div>
                        <p><a href="#">会社概要</a></p>
                    </div>
                    <div>
                        <p><a href="#">会社沿革</a></p>
                    </div>
                    <div>
                        <p><a href="#">研修活動</a></p>
                    </div>
                    <div>
                        <p><a href="#">展示会</a></p>
                    </div>
                    <div>
                        <p><a href="#">出版物・カタログ</a></p>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <p class="c-navi__item"><a href="#"></a>営業所一覧</p>
        </li>
        <li class="c-navi__sub">
            <p class="c-navi__item"><a href="#"></a>採用情報</p>
            <div class="c-navi__child">
                <div class="c-navi__child__title">採用情報</div>
                <div class="l-container">
                    <div>
                        <p><a href="#">採用情報</a></p>
                    </div>
                    <div>
                        <p><a href="#">新卒募集要項</a></p>
                    </div>
                    <div>
                        <p><a href="#">キャリア採用情報</a></p>
                    </div>
                    <div>
                        <p><a href="#">採用Q&amp;A</a></p>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <p class="c-navi__item"><a href="#"></a>お知らせ</p>
        </li>
        <li>
            <p class="c-navi__item"><a href="#"></a>お問い合わせ</p>
        </li>
    </ul>
</nav>